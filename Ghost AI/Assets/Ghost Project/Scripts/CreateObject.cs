using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateObject : MonoBehaviour
{

    public Transform Spawnpoint;
    public GameObject Prefab;
    public float destroyDelay = 3f;//v3 editable delay on destroy object

    void OnTriggerEnter()
    {
        Prefab = Instantiate(Prefab, Spawnpoint.position, Spawnpoint.rotation);//Spawns Prefab
        Destroy(Prefab, destroyDelay);//destroys object after specified delay

        //Destroy(Prefab, 3); //v2 destroys Prefab after 3 seconds of triggering
    }

    /*
    void OnTriggerExit()
    {
        Destroy(Prefab); //v1 destroyed Prefab on exit of trigger event
    }
    */
}
